#! /usr/bin/python3
import pymongo
import sys

conn = pymongo.MongoClient('192.168.0.81', 27017)
db = conn['brain']
coll = db['userapps']

pipe = sys.stdin.readline()
while(pipe):
    pipe = pipe.replace('\n', '')
    ret = coll.find({'_id' : pipe})
    for field in ret:
        for pname in field:
            click = int(field[pname])
            if click < 2 or pname == "_id":
                continue
            print(pname, click)
    pipe = sys.stdin.readline()
