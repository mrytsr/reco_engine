#! /bin/bash

mkdir -p ./pnames
for pname in `awk -F',' '{print $4}' ./pack.txt|sort|uniq -c|sort -gk1|grep -o '[0-9]\+ [a-zA-Z.]\+'|awk '{print $2}'|tail -20`;
do
    echo $pname
    grep "${pname}" ./pack.txt |awk -F',' '{print $2}'|sort|uniq|./mongo.py > ./pnames/$pname
done
